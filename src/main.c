#include "mem.h"
#include <stdint.h>
#include <stdio.h>

uint32_t msize = 32;

void test1(){ // простое выделение
    printf("Тест #1\n");
    uint32_t* a = _malloc(msize);
    debug_heap(stdout, HEAP_START);
    _free(a);
}

void test2(){ // освобождаем 1 переменную
    printf("Тест #2\n");
    char* a = _malloc(msize);
    char* b = _malloc(msize);
    char* c = _malloc(msize);
    char* d = _malloc(msize);
    _free(b);
    debug_heap(stdout, HEAP_START);
    _free(a);
    _free(c);
    _free(d);
}

void test3(){ // освобождаем 2 переменную
    printf("Тест #3\n");
    char* a = _malloc(msize);
    char* b = _malloc(msize);
    char* c = _malloc(msize);
    char* d = _malloc(msize);
    _free(b);
    _free(d);
    debug_heap(stdout, HEAP_START);
    _free(a);
    _free(c);
}

void test4(){ // новый регион расширяет старый
    printf("Тест #4\n");
    char* a = _malloc(msize - 1);
    debug_heap(stdout, HEAP_START);
    
    char* b = _malloc(1);
    debug_heap(stdout, HEAP_START);
    _free(a);
    _free(b);
}

void test5(){ // новый регион расширяет старый
    printf("Тест #5\n");
    heap_init(0);
    debug_heap(stdout, HEAP_START);
    
    char* a = _malloc(msize);
    debug_heap(stdout, HEAP_START);
    _free(a);
}


int main (){
    heap_init(0);
    test1();
    test2();
    test3();
    test4();
    test5();

    return 0;
}
